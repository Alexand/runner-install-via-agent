# Install GitLab Runner via the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent)

These instructions consider that the agent is already configured as per the [GitLab Kubernetes Agent](https://docs.gitlab.com/ee/user/clusters/agent) documentation.

1. Check the possible Runner chart yaml values on the Runner chart documentation and create a `runner-chart-values.yaml` file with the configuration that fits your needs. E.g.:

    ```yaml
    ## The GitLab Server URL (with protocol) that want to register the runner against
    ## ref: https://docs.gitlab.com/runner/commands/README.html#gitlab-runner-register
    ##
    gitlabUrl: https://gitlab.my.domain.com/
    
    ## The Registration Token for adding new Runners to the GitLab Server. This must
    ## be retrieved from your GitLab Instance.
    ## ref: https://docs.gitlab.com/ce/ci/runners/README.html
    ##
    runnerRegistrationToken: "XXXXXXXXXXXXXXXXXXXXXXYYYYYYYYYYYYYYYYYYYYYZZZZZZZZZZZZZZZZZZZZZ"
    
    ## For RBAC support:
    rbac:
      create: true
    
    ## Run all containers with the privileged flag enabled
    ## This will allow the docker:dind image to run if you need to run Docker
    ## commands. Please read the docs before turning this on:
    ## ref: https://docs.gitlab.com/runner/executors/kubernetes.html#using-dockerdind
    runners:
      privileged: true
    ```  

1. Create a single manifest file to install the Runner chart with your cluster agent

   ```bash
   helm template --namespace gitlab gitlab-runner -f runner-chart-values.yaml gitlab/gitlab-runner > runner-manifest.yaml
   ```

1. Push your `runner-manifest.yaml` to your manifest repository.
